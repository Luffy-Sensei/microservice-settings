package com.tenor.sf.jobflow.settings.dao;


import com.tenor.sf.jobflow.settings.domain.Setting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingRepository extends JpaRepository<Setting, Long>{

}
