package com.tenor.sf.jobflow.settings.service;

import com.tenor.sf.jobflow.settings.dao.SettingRepository;
import com.tenor.sf.jobflow.settings.domain.Setting;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.annotations.GraphQLArgument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

//TODO: How to check permissions
@Service
public class SettingService {

    private final SettingRepository repository;

    @Autowired
    public SettingService(SettingRepository repository) {
        this.repository = repository;
    }

    @GraphQLQuery(name = "findAll")
    public List<Setting> findAll() {
        return repository.findAll();
    }

    @GraphQLQuery(name = "getSettingById")
    public Optional<Setting> getSettingById(@GraphQLArgument(name = "id") Long id) {
        return repository.findById(id);
    }

    @GraphQLMutation(name = "saveSetting")
    public Setting saveSetting(@GraphQLArgument(name = "setting") Setting setting) {
        return repository.save(setting);
    }

    @GraphQLMutation(name = "deleteSetting")
    public void deleteSetting(@GraphQLArgument(name = "id") Long id) {
        repository.deleteById(id);
    }
}
