package com.tenor.sf.jobflow.settings.domain;

import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Getter @Setter
@NoArgsConstructor
@ToString
public class Setting {

    @Id
    @GeneratedValue
    @GraphQLQuery(name = "id", description = "A setting param's id")
    private Long id;

    @GraphQLQuery(name = "name", description = "A setting param's name")
    @Column(nullable = false)
    private String name;

    @GraphQLQuery(name = "value", description = "A setting param's value")
    @Column(nullable = false)
    private String value;

    @GraphQLQuery(name = "creationDate", description = "The creation date of a param")
    private LocalDate creationDate;
}
